# Conditional HTML in Bootstrap

This example shows how to use webpack to bundle Bootstrap and use a different
`index.html` structure in development and production modes. The purpose is to
prevent a flash of unstyled content (FOUC) in development mode where it may be
advantageous to load CSS dynamically from `main.js`. In this case, `main.js` is
loaded at the start of `<body>` to prevent a FOUC. Otherwise, it's loaded at the
end so the script won't delay the content.

The web page is based on the Bootstrap [starter
template](https://getbootstrap.com/docs/4.5/examples/starter-template/) example.
[Handlebars.js](https://www.npmjs.com/package/handlebars) is used to handle the
logic inside `index.html`. Bootstrap is bundled by compiling the source SCSS
files.

## Installation

``` bash
npm install
npx webpack --mode=<mode> # development or production
```

Then load `dist/index.html` in a browser.

## In Development and Other Non-Production Modes

* Does not extract CSS to a separate file, uses `style-loader` instead
* Does not insert `<link href="./main.css" rel="stylesheet">`
* Inserts `<script src="./main.js"></script>` at the _beginning_ of `<body>`

Rather than extract CSS into a file it's loaded dynamically by `main.js`. This
allows you do things like [hot module
replacement](https://webpack.js.org/concepts/hot-module-replacement/). As
mentioned in the [mini-css-extract-plugin
repository](https://github.com/webpack-contrib/mini-css-extract-plugin):

> [`mini-css-extract-plugin`] should be used only on production builds without
> style-loader in the loaders chain, especially if you want to have HMR in
> development.

We load `main.js` at the _beginning_ of `<body>` so that we won't have a flash
of unstyled content (FOUC) in development. When `main.js` is inserted at the end
of `<body>`, the delay in applying the CSS from the large `main.js` script
causes the FOUC. If you don't mind having a FOUC in development, then there's no
need to do all this.

## In Production Mode

* Extracts CSS `main.css` using `mini-css-extract-plugin`
* Inserts `<link href="./main.css" rel="stylesheet">`
* Inserts `<script src="./main.js"></script>` at the _end_ of `<body>`
