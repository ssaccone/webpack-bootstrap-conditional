const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Handlebars = require('handlebars');

module.exports = (env, argv) => {
    const prodMode = argv.mode === 'production';

    const config = {
        mode: 'development',
        entry: './src/index.js',
        output: {
            filename: 'main.js',
            path: path.resolve(__dirname, 'dist'),
        },
        plugins: [
            new CleanWebpackPlugin(),
            new MiniCssExtractPlugin()
        ],
        module: {
            rules: [
                {
                    test: /\.(scss)$/,
                    use: [{
                        loader: prodMode ? MiniCssExtractPlugin.loader : 'style-loader',
                    }, {
                        loader: 'css-loader', // translates CSS into CommonJS modules
                    }, {
                        loader: 'postcss-loader', // run postcss actions
                        options: {
                            plugins: function () { // postcss plugins, can be exported to postcss.config.js
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    }, {
                        loader: 'sass-loader' // compiles Sass to CSS
                    }]
                },
                {
                    test: /\.hbs$/i,
                    use: [
                        'file-loader?name=[name].html', // html not [ext]
                        'extract-loader',
                        {
                            loader: 'html-loader',
                            options: {
                                attributes: false, // leave <script>, etc, intact
                                preprocessor: (content, loaderContext) => {
                                    let result;

                                    try {
                                        result = Handlebars.compile(content)({
                                            prodMode: prodMode,
                                        });
                                    } catch (error) {
                                        loaderContext.emitError(error);
                                        return content;
                                    }

                                    return result;
                                },
                            }
                        }
                    ],
                }
            ]
        }
    };

    return config;
};
